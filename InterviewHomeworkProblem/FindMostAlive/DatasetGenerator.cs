﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace FindMostAlive
{
    public class DatasetGenerator
    {
        /// <summary>
        /// Generate a random dataset and write it out as a JSON file with a given number of elements
        /// </summary>
        /// <param name="filename">The filenamne to write to</param>
        /// <param name="elements">The number of peopel to generate in the dataset</param>
        public static void Generate( string filename, int elements )
        {
            //Representing birth and end years as a list tuples
            var dataSet = new List<Tuple<int, int>>();
            //Create a random number generator
            var random = new Random();

            for( int i = 0; i < elements; ++i )
            {
                //Creates a number between 1900 and 2000 (per docs one more than top number)
                var birthYear = random.Next( 1900, 2001 );
                //Creates a random number from birthYear to 2000
                var endYear = random.Next( birthYear, 2001 ); 

                //Add the data set to the list.
                dataSet.Add( new Tuple<int, int>( birthYear, endYear ) );
            }

            //Serialize the data set to a file
            var json = JsonConvert.SerializeObject( dataSet );

            File.WriteAllText( filename, json );
        }
    }
}
