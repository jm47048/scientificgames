﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace FindMostAlive
{
    public class DatasetProcessor
    {
        static int START = 1900;        
        
        /// <summary>
        /// Processes the data set and returns the results.  Algorithm assumes population at the end of the year.  It also assumes
        /// all births and deaths occur between 1900 and 2000.
        /// </summary>
        /// <param name="filename">The file where the input dataset is stored</param>
        /// <returns>A key value pair with the year list and the max population</returns>
        public static KeyValuePair<List<int>, int> Process( string filename )
        {
            var fileData = File.ReadAllText( filename );

            //We have a data set which is a list of tuples.  Item1 is birth year.  Item2 is end year.
            var dataSet = JsonConvert.DeserializeObject<List<Tuple<int, int>>>( fileData );

            //Init an array where 1900 + index is the year and 101 so 2000 inclusive.
            var years = new int[ 101 ];
            Array.Clear( years, 0, years.Length );

            //Walk through everyone in the dataset.  You have to loop to insepct each field.
            foreach( var person in dataSet )
            {
                //Create the indexes into the array
                var birthIndex = person.Item1 - START;
                var endIndex = person.Item2 - START;

                //Increment the counter when a birth has occured
                years[ birthIndex ]++;
                //Decrement the counter when a death has occured
                years[ endIndex ]--;
            }

            //Now start the population counter at years of 0.  Which is our "current" max.
            int maxPopulation = years[ 0 ];
            //Also the population counter starts at the first element
            int populationCount = years[ 0 ];

            //Create a list to store the years with the max population.
            var yearList = new List<int>();

            //Now walk through the array of years
            for( int i = 1; i < years.Length; i++ )
            {
                //Start counting the population as it grows and shrinks
                populationCount += years[ i ];

                //Check if we have grown above the max population
                if( populationCount > maxPopulation )
                {
                    //If we have then we set the new max population count
                    maxPopulation = populationCount;

                    //We have a new max population so clear the list.
                    yearList.Clear();
                }

                //We need to get all the years where we have this max population.  This covers ties.
                if (populationCount == maxPopulation)
                {
                    yearList.Add( i + 1900 );
                }
            }

            //Now that we have the max population and the list of years we can return the results.
            return new KeyValuePair<List<int>, int>(yearList, maxPopulation);
        }        
    }
}