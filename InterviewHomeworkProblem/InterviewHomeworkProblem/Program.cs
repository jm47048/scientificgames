﻿using FindMostAlive;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

namespace InterviewHomeworkProblem
{
    /// <summary>
    /// The main program.  It will read the data set from a JSON file and send it to the processor.
    /// </summary>
    class Program
    {
        static string FILENAME = @"c:\temp\testFile.json";
        
        /// <summary>
        /// Main entry point.
        /// </summary>
        static void Main()
        {
            //Generate a dataset to a file.  The requirements of the problem ask to submit the sample dataset.
            DatasetGenerator.Generate( FILENAME, 1000 ); 

            //Process the dataset and return a list of max population years.  This accounts for ties.
            var results = DatasetProcessor.Process( FILENAME );

            //Write a report to a text file.
            var builder = new StringBuilder();
            builder.Append("********** OUTPUT REPORT **********");
            builder.Append(Environment.NewLine);
            builder.Append("MAX POPULATION: " + results.Value );
            builder.Append( Environment.NewLine );
            builder.Append( "YEARS:" + Environment.NewLine );

            foreach( var year in results.Key )
            {
                builder.Append( year + Environment.NewLine );
            }

            File.WriteAllText( @"c:\temp\outputFile.txt", builder.ToString() );
        }
    }
}
