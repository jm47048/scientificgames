# Problem 1

Description
Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most number of people alive.

Code
Solve using a language of your choice and dataset of your own creation.

Submission
Please upload your code, dataset, and example of the program�s output to Bit Bucket or Github. Please include any graphs or charts created by your program.

# Solution

A JSON dataset is randomly generated and written to a file.  A processor then reads from that file and begins.  Processing happens by creating an array of 101 elements (one more for 2000 inclusive). Based on the problem 
statement we assume that all births and all end years occur within the years 1900 and 2000.  That would mean that population growth would begin at 1900, and return to 0 at 2000.  This now becomes an exercise in increment and 
decrement as each birth and end are discovered in the dataset.  As we walk through the dataset we add one to a year�s counter for each birth, and subtract one for each end-year at the element representing the given year.  

Once this is complete, we can walk through the years and add the value in each year (some may be negative).  As the population increases the new max population will be considered, and with each time the max population is equal to the
currently calculated population we capture the year.  If the max population changes, then we need to clear the list as a new leader has been found.

Finally we return the list of years (ties are possible) and the max population and write a small text file with a report on the findings.  The report includes the calculated max population and the year or years that 
population was discovered.  

Timing is O(n + m) where n is the number of persons, and m is the number of years.  So the solution, although possibly not optimal, is better than the brute force approach.

The algorithm only considers population totals at the �end� of the year.  Dates are not considered in this algorithm.

The solution was written in C# in Visual Studio 2013 Community, and is a simple console application.

At the root of the repository is a testFile.json which served as the input set, randomly generated, 1000 elements.  There is also an outputFile.txt, which represents a simple report to show the max population and the year(s).